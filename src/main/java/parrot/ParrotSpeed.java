package main.java.parrot;

public interface ParrotSpeed {
	public double calculateParrotSpeed(Parrot parrot);
}
