package main.java.parrot;

public class Parrot {

    private ParrotTypeEnum type;
    private int numberOfCoconuts = 0;
    private double voltage;
    private boolean isNailed;


    public Parrot(ParrotTypeEnum _type, int numberOfCoconuts, double voltage, boolean isNailed) {
        this.type = _type;
        this.numberOfCoconuts = numberOfCoconuts;
        this.voltage = voltage;
        this.isNailed = isNailed;
    }

    
    public ParrotSpeed getParrotSpeed() {
    	ParrotSpeed parrot = null;    	
    	if (type == ParrotTypeEnum.EUROPEAN) {
    		parrot = new EuropeanParrot();
		}
    	if (type == ParrotTypeEnum.AFRICAN) {
        	parrot = new AfricanParrot(numberOfCoconuts);    	
		}
    	if (type == ParrotTypeEnum.NORWEGIAN_BLUE) {
    	 	parrot = new NorwegianBlueParrot(isNailed, voltage);
    	}
    	return parrot;
    }
    
    public double getSpeed() {
        double TotalSpeedParrot;
        ParrotSpeed parrot = getParrotSpeed();
        TotalSpeedParrot = parrot.calculateParrotSpeed(this);
        return TotalSpeedParrot;
    }

    public double getBaseSpeed(double voltage) {
        return Math.min(24.0, voltage*getBaseSpeed());
    }

    public double getLoadFactor() {
        return 9.0;
    }

    public double getBaseSpeed() {
        return 12.0;
    }


}
