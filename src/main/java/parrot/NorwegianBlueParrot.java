package main.java.parrot;

public class NorwegianBlueParrot implements ParrotSpeed {
	
	boolean isNailed;
    double voltage;
    
	NorwegianBlueParrot(boolean isNailed,double voltage) {
        this.voltage = voltage;
        this.isNailed = isNailed;
    }
	@Override
	public double calculateParrotSpeed(Parrot parrot) {
		return (isNailed) ? 0 : parrot.getBaseSpeed(voltage);
	}

}
