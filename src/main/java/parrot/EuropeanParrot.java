package main.java.parrot;

public class EuropeanParrot implements ParrotSpeed{

	@Override
	public double calculateParrotSpeed(Parrot parrot) {
		return parrot.getBaseSpeed();
	}

}
