package main.java.parrot;

public class AfricanParrot implements ParrotSpeed {

    int numberOfCoconuts;
	
	public AfricanParrot(int numberOfCoconuts) {
        this.numberOfCoconuts = numberOfCoconuts;
	}
	
	@Override
	public double calculateParrotSpeed(Parrot parrot) {
		return Math.max(0, parrot.getBaseSpeed() - parrot.getLoadFactor() * numberOfCoconuts);
	}

}